Categories:Games,Action
License:GPLv3
Web Site:https://bitbucket.org/przemekr/dot-race
Source Code:https://bitbucket.org/przemekr/dot-race/src
Issue Tracker:https://bitbucket.org/przemekr/dot-race/issues

Auto Name:Dot-Race
Summary:Simple, funny action game.
Description:
Dot race is simple, but funny and fast action game. Take the control of your dot
and compete with computer or an other player in collecting magic artifacts.
.

Repo Type:git
Repo:https://bitbucket.org/przemekr/dot-race

#Repo:file:///home/eprzrze/Projects/dot-race
Build:0.1,1
    commit=0.1
    subdir=android
    srclibs=SDL@release-2.0.3
    prebuild=rm -rf jni/SDL/src jni/SDL/include && \
        ln -s ../../$$SDL$$/src     jni/SDL/ && \
        ln -s ../../$$SDL$$/include jni/SDL/ && \
        echo "" > jni/SDL/src/dynapi/SDL_dynapi.h
    buildjni=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1
Current Version Code:1

