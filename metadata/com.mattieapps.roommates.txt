Categories:Office
License:Apache2
Web Site:
Source Code:https://github.com/mattieapps/roommates
Issue Tracker:https://github.com/mattieapps/roommates/issues

Auto Name:RoomMates
Summary:Rent and tip calculator
Description:
Split bills among groups of people like roommates.
.

Repo Type:git
Repo:https://github.com/mattieapps/roommates

Build:1.1.1,3
    commit=71dfc4e0da9bcc6c227e36544affd2c73dc207a1
    subdir=roommates-android/app
    gradle=yes
    prebuild=rm ../local.properties

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.2
Current Version Code:5

